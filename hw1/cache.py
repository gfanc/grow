import functools


def cache_decorator(func):
    """
    Decorator to wrap a function with a caching callable that saves up to most recent calls.
    """
    cache = {}

    @functools.wraps(func)
    def inner(*args):
        if args in cache:
            return cache[args]
        result = func(*args)
        cache[args] = result
        return result

    return inner
