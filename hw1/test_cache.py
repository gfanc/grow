from unittest.mock import Mock

from hw1.cache import cache_decorator


def test_cache_decorator():
    """
    Test cache decorator with different arguments.
    """
    mock = Mock()
    cached_function = cache_decorator(mock)

    # 1
    cached_function(1)
    cached_function(1)

    # 2
    cached_function(1, 2, 3, 4)
    cached_function(1, 2, 3, 4)

    assert mock.call_count == 2
